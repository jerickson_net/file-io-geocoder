# Java File IO Review using Google Geocoder API

Code based partially on http://nifty.stanford.edu/2015/reges-geolocator/

Includes classes written by the Cleveland and Columbus Tech Elevator students.

The Geocoder was rewritten to be clearer about how it uses the XML classes
to parse the results returned from Google. It's still a lot of XML and XPath,
but it might be clearer about how those tools are used.

You can find out more about the Google Maps Geocoding API at
https://developers.google.com/maps/documentation/geocoding/start

To learn more about XPath and Java, you can look here:
http://www.javamex.com/tutorials/xml/parsing_xpath.shtml (This is,
honestly, the least dry tutorial I could find.)
