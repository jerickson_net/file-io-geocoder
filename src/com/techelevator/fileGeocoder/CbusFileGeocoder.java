package com.techelevator.fileGeocoder;

import java.io.File;
import java.io.PrintWriter;
import java.util.Scanner;

public class CbusFileGeocoder {

	public static void main(String[] args) {
		// Ask user for a file path
		System.out.print("What's the filename of the address file: ");
		
		Scanner console = new Scanner(System.in);
		
		String filepath = console.nextLine();
		
		GeoCoder geocoder = new GeoCoder();
		
		// Open the file
		try (
				Scanner addressFile = new Scanner(new File(filepath));
				PrintWriter outFile = new PrintWriter(filepath + ".geo");		
		){
			
			while(addressFile.hasNextLine()){
				String address = addressFile.nextLine();
				
				GeoLocation latLong = geocoder.find(address);
				
				outFile.println(address + latLong.toString());
			}
		} catch (Exception e) {
			System.out.println("File not found");
			System.exit(1);
		}
		
		
	}

}
