package com.techelevator.fileGeocoder;

//This class stores information about a location on Earth.  Locations are
//specified using latitude and longitude.  The class includes a method for
//computing the distance between two locations.

public class GeoLocation {
 private double latitude;
 private double longitude;

 // constructs a geo location object with given latitude and longitude
 public GeoLocation(double theLatitude, double theLongitude) {
     latitude = theLatitude;
     longitude = theLongitude;
 }

 // returns the latitude of this geo location
 public double getLatitude() {
     return latitude;
 }

 // returns the longitude of this geo location
 public double getLongitude() {
     return longitude;
 }

 // returns a string representation of this geo location
 public String toString() {
     return "latitude: " + latitude + ", longitude: " + longitude;
 }
}
