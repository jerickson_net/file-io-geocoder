package com.techelevator.fileGeocoder;

import static javax.xml.xpath.XPathConstants.NUMBER;
import static javax.xml.xpath.XPathConstants.STRING;

//This class provides two static methods called find and findPlace that
//provide access to the Google Maps api.  They take a search string and return
//the latitude and longitude of the top hit for the search (find) along with
//metadata (findPlace).  Returns null if the search produces no results or if
//no internet connection is available.
//
//Sample calls:
//  GeoLocation location = GeoLocator.find("space needle");
//  PlaceInformation place = GeoLocator.findPlace("space needle");
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLEncoder;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

public class GeoCoder {
// URL for hitting the Google maps api
private String queryUrlPrefix = 
    "https://maps.googleapis.com/maps/api/geocode/xml?sensor=false&address=";

//Needed to encode the given query string to be used in the url
private String charEncoding = "UTF-8";

//For parsing the XML documents that are returned by the Maps API:
private DocumentBuilder docBuilder;

private XPathExpression firstResultXpath;
private XPathExpression latXpath;
private XPathExpression lngXpath;

private String firstResultPath = "/GeocodeResponse/result[1]";
private String latPath = "/GeocodeResponse/result[1]/geometry/location/lat";
private String lngPath = "/GeocodeResponse/result[1]/geometry/location/lng";

public GeoCoder() {
	   // create the DocumentBuilder
	   DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
	   DocumentBuilder docBuilder = null;
	   try {
	      docBuilder = documentBuilderFactory.newDocumentBuilder();
	   } catch (ParserConfigurationException pce) {
	      handleException(pce);
	   }
	   
	   this.docBuilder = docBuilder;
	   
	   // precompile the XPathExpressions
	   XPathFactory xPathFactory = XPathFactory.newInstance();
	   XPath xPath = xPathFactory.newXPath();
	   
	   firstResultXpath = compile(xPath, firstResultPath);
	   latXpath = compile(xPath, latPath);
	   lngXpath = compile(xPath, lngPath);
}
 // Given a query string, returns a GeoLocation representing the coordinates
 // of Google's best guess at what the query string represents.
 //
 // Returns null if there are no results for the given query, or if we
 // cannot connect to the Maps API.
public GeoLocation find(String query) {
   // call google maps api, and parse results
   Document parsedLocationXML = parseQueryResultsIntoDocument(query);
   
   // bail if no results
   if (!containsLocationResult(parsedLocationXML)) {
      return null;
   }
   
   // pull geolocation from top result
   return find(parsedLocationXML);
}



// accepts a Google Maps API XML document, returns whether it contains at
// least one search result
private boolean containsLocationResult(Document parsedLocationXML) {

   try {
      String result = (String) firstResultXpath.evaluate(parsedLocationXML, STRING);
      return null != result && !"".equals(result);
   } catch (XPathExpressionException xpee) {
      // Malformed or unrecognized xml
      handleException(xpee);
   }
   return false;
}

// accepts Google Maps API XML document, returns the latitude and longitude
// of the top search result, or null if none found
private GeoLocation find(Document parsedLocationXML) {
   try {
      Double lat = (Double) latXpath.evaluate(parsedLocationXML, NUMBER);
      Double lng = (Double) lngXpath.evaluate(parsedLocationXML, NUMBER);
      return new GeoLocation(lat, lng);
   } catch (XPathExpressionException xpee) {
      // Malformed or unrecognized xml
      handleException(xpee);
   }
   return null;
}

// given a search query, returns a Google Maps API document representing the search results
private Document parseQueryResultsIntoDocument(String query) {
   try {
	   String url = queryUrlPrefix + urlEncodeUTF8(query);
	   Document result = docBuilder.parse(new URL(url).openStream());
      return result;
   } catch (SAXException saxe) {
      handleException(saxe);
   } catch (IOException ioe) {
      handleException(ioe);
   }
   
   return null;
}

// Defines how to handle any exceptions in this program -- probably can be empty.
private static void handleException(Exception e) {
   System.out.println(e);
}

// URL-encodes the given String, swallows impossible exception.
private String urlEncodeUTF8(String toEncode) {
   try {
      return URLEncoder.encode(toEncode, charEncoding);
   } catch(UnsupportedEncodingException uee) {
      // Won't happen, encoding is hardcoded to a known working value.
      System.err.println("Unable to encode, charset " + charEncoding + " is unsupported.");
      return toEncode;
   }
}

// Uses the given XPath object and xpath string to create an XPathExpression
// (Its purpose here is to swallow any exceptions)
private XPathExpression compile(XPath xPath, String path) {
   try {
      return xPath.compile(path);
   } catch (XPathExpressionException xpee) {
      handleException(xpee);
   }
   return null;
}
}
