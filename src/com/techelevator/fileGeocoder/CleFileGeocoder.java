package com.techelevator.fileGeocoder;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

public class CleFileGeocoder {
	public static void main(String[] args) {
		// Ask for filename
		System.out.print("What's the filename of the address file: ");
		Scanner console = new Scanner(System.in);
		String filename = console.nextLine();
		
		File inputFile = new File(filename);
		
		if(! inputFile.exists() || ! inputFile.isFile()) {
			System.out.println("File " + filename + " doesn't exist.");
			System.exit(1);
		}
		
		GeoCoder geocoder = new GeoCoder();
		
		try (
				Scanner addressFile = new Scanner(inputFile);
				PrintWriter latLongFile = new PrintWriter(filename + ".geo")
		) {
			while(addressFile.hasNextLine()) {
				String address = addressFile.nextLine();
				GeoLocation location = geocoder.find(address);
				
				latLongFile.println(address + ", " + location);
			}
		} catch (FileNotFoundException e) {
			// This should never happen
			System.out.println("File not found");
		}
		
		System.out.println("Done.");

		
		// Scan file line by line
		
		// Send to google
		
		// Get lat and long
		
		// Write to another file
	
	}
}
