package com.techelevator.fileGeocoder;

import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class FinishedGeocoder {

	public static void main(String[] args) throws IOException {
		// TODO get file path from user
		System.out.print("Enter a file name that contains addresses to geocode: ");
		Scanner consoleScanner = new Scanner(System.in);
		String filename = consoleScanner.nextLine();
		
		// TODO read file line by line and pass to geocoder
		Scanner fileScanner = new Scanner(new FileReader(filename));
		PrintWriter geocoded = new PrintWriter(filename + ".geo");
		int count = 0;
		
		GeoCoder geocoder = new GeoCoder();
		
		while(fileScanner.hasNextLine()) {
			String line = fileScanner.nextLine();
			
			GeoLocation latAndLong = geocoder.find(line);
			
			// https://geocoding.geo.census.gov/geocoder/locations/onelineaddress?address=4600+Silver+Hill+Rd%2C+Suitland%2C+MD+20746&benchmark=9&format=json
			
			// TODO get results and write them to new file
			if(latAndLong != null) {
				geocoded.write(
					line + " (" + latAndLong.getLatitude() + ", " + latAndLong.getLongitude() + ")\n"
				);
			}
			
			// TODO tell user how many addresses we did
			count++;
		}
		
		System.out.println("We geocoded " + count + " addresses.");
		
		fileScanner.close();
		geocoded.close();
		consoleScanner.close();
	}

}
